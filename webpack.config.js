var glob = require('glob');

module.exports = {
    entry: {
        app: glob.sync(__dirname + '/src/js/*.js')
    },
    watch:true,
    output: {
        path: __dirname,
        filename: 'script.js',
        publicPath:'/'
    },
    module: {
        rules: [
            {
                test: /\.js$/, 
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    presets:['es2015','react']
                }
            },{
                test:/\.css$/,
                loader: "style-loader!css-loader"
            }
        ]
    },
    devServer:{
        historyApiFallback:true
    }
}