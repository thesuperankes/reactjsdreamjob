import React, { Component } from "react";
import TodoList from "./List";
import { Redirect, Router } from 'react-router-dom';
import "../css/index.css";
import history from './history'

var IsLogged = false;

class App extends Component {

    constructor(props) {
        super(props);

        this.state = { email: '', password: '' }

        this.handleSubmit = this.handleSubmit.bind(this);


    }

    login() {
        if (this.state.email == "admin@admin.com" && this.state.password == "123456") {
            localStorage.setItem("IsLogged", true);
            this.IsLogged = true;
            history.push('/todo');
            window.location.reload();
        } else {
            alert("Usuario no encontrado");
        }
    }

    handleSubmit(event) {
        this.login();
        event.preventDefault();
    }

    render() {
        return (
            <div className="list">
                <div className="head">
                    <form onSubmit={this.handleSubmit}>
                        <input type="email" id="email" value={this.state.email}
                            placeholder="Ingrese Correo"
                            onChange={e => this.setState({ email: e.target.value })} /><br />

                        <input type="password" id="password" value={this.state.password}
                            placeholder="Ingrese Contraseña"
                            onChange={e => this.setState({ password: e.target.value })} /><br />

                        <input type="submit" value="Submit" />
                    </form>
                </div>
            </div>
        );
    }
}

export default App;