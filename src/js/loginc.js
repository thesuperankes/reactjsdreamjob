import React, { Component } from 'react';
import ReactDOM from "react-dom";
import TodoList from "./List";
import App from "./todoList";
import "../css/index.css";
import { Route, BrowserRouter as Router } from 'react-router-dom';
import history from './history'


var destination = document.querySelector("#container");

const User = ({ match }) => {
    return (<h1> Welcome {match.params.username}</h1>);
}

var isLogged = localStorage.getItem("IsLogged");

function requireAuth(){
    if(localStorage.getItem("IsLogged") === "true"){
        console.log("True Bro");
        return true;
    }else{
        console.log("False bro");
        return false;
    }
}

ReactDOM.render(
    <Router history={history}>
        <div>
            <Route path="/" exact component={App} />
            <Route path="/todo" exact render={()=>{
                return requireAuth() === true ? (
                    <TodoList/>
                ): <App/>
            }} />
        </div>
    </Router>

    , destination

);