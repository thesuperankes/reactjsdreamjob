import React, { Component } from 'react';
import TodoItems from "./Item"
import "../css/list.css";

class TodoList extends Component {
  constructor(props) {
    super(props);


    this.state = {
      items: []
    };



    this.addItem = this.addItem.bind(this);
    this.deleteItem = this.deleteItem.bind(this);
  }

  addItem(e) {
    if (this._inputElement.value !== "") {
      var newItem = {
        text: this._inputElement.value,
        key: Date.now()
      };



      this.setState((prevState) => {
        localStorage.setItem("items", JSON.stringify(prevState));
        return {
          items: prevState.items.concat(newItem)
        };

      });
    }

    this._inputElement.value = "";

    console.log(this.state.items);

    e.preventDefault();
  }

  deleteItem(key) {
    var item = this.state.items.filter(function (item) {
      return (item.key !== key)
    });
    this.setState({
      items: item
    });
  }

  render() {
    return (
      <div className="list">
        <div className="head">
          <form onSubmit={this.addItem}>
            <input ref={(a) => this._inputElement = a}
              placeholder="Ingrese Tarea"></input>
            <button type="submit">Agregar a la lista</button>
          </form>
        </div>
        <TodoItems entries={this.state.items} delete={this.deleteItem} />
      </div>
    );
  }
}
export default TodoList;